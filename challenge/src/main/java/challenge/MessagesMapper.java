package challenge;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class MessagesMapper implements RowMapper<Messages> {

	@Override
	public Messages mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		
		Messages message = new Messages(); 
		message.setId(rs.getInt("id"));
		message.setPerson_id(rs.getInt("person_id"));
		message.setContent(rs.getString("content"));
		
		return message;
	}

}
