package challenge;

import java.security.Principal;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FollowerController {
    @Autowired
    private Database database;


    
  // Getting the follower list of current user  
    @RequestMapping(value = "/followerslist", method = RequestMethod.GET)
    public List<People> getFollowers(HttpServletRequest request) {
    	Principal principal = request.getUserPrincipal();
    	Integer id = Integer.parseInt(principal.getName());
         return database.getFollowers(id);
    }
   // Getting the list of people that current user following 
    @RequestMapping(value = "/followinglist", method = RequestMethod.GET)
    public List<People> getFollowing(HttpServletRequest request) {
    	Principal principal = request.getUserPrincipal();
    	Integer id = Integer.parseInt(principal.getName());
        return database.getFollowingList(id);
    }  
    

    
    //Post Service: Post a JSON current user wants to follow in format of key ""followId", Value: "userId"
    //{
    //"followId":"5"
    //}
    //
    @RequestMapping(value = "/follow", method = RequestMethod.POST)
    public String Following(@RequestBody String body) {
    	 Authentication auth = SecurityContextHolder.getContext().getAuthentication();
         String username = auth.getName();
         JSONObject json =  new JSONObject(body);
    	Integer id = Integer.parseInt(username);
        database.following(id, Integer.valueOf(json.get("followId").toString()));
        return "Followed UserId: '" + json.get("followId").toString() +"' successfully !" ;
    }
    
    //Post Service: Post a JSON current user wants to unfollow in format of key ""followId", Value: "userId"
    //{
    //"followId":"5"
    //}
    //

    @RequestMapping(value = "/unfollow", method = RequestMethod.POST)
    public String unFollowing(@RequestBody String body) {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        JSONObject json =  new JSONObject(body);
        Integer id = Integer.parseInt(username);
        database.deleteUserFollowing(id, Integer.valueOf(json.get("unfollowId").toString()));
        return "unFollowed UserId: '" + json.get("unfollowId").toString() +"' successfully !";
    }
    
    //Extra feature 2: Getting the userlist : the most popular follower
    @RequestMapping(value = "/popularfollowerlist", method = RequestMethod.GET)
    public List<PopularFollowers> getMostPoplularFollowers(HttpServletRequest request) {
        return database.getPopularFollowers();
    }  
    
    
    //Extra feature 3: returned data that contains each user's number of followers and number of users they follow
    //The data format is fixed for the nvd3 multi-bar chart visualizing the data
    @RequestMapping(value = "/userstats", method = RequestMethod.GET)
    public String UserIdWithNumOfFollowers(HttpServletRequest request) {    	
    	List<UserIdStats>  followerNum = database.getUserIdWithNumOfFollowers();
    	JSONObject followerrst  = new JSONObject();
    	JSONObject follower  = new JSONObject();
    	follower.put("list", followerNum);
    	JSONArray array = follower.getJSONArray("list");
    	followerrst.put("key", "Followers");
    	followerrst.put("values", array);
    	
    	List<UserIdStats>  followingNum = database.getUserIdWithNumOfFollowering();
    	JSONObject followingrst  = new JSONObject();
    	JSONObject following  = new JSONObject();
    	following.put("list", followingNum);
    	JSONArray array2 = following.getJSONArray("list");
    	followingrst.put("key", "Following");
    	followingrst.put("values", array2);
    	
    	JSONArray result = new JSONArray();
    	result.put(0, followerrst);
    	result.put(1, followingrst);
    	
         return result.toString();
    }
    
    
}
