package challenge;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class Database {
	  @Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	//query the messages current user sent and sent by users they follow
	//First check if search parameter is null,if not, query the message content that match the search word  
    @Transactional
	public List<Messages> getUserMessages(Integer id, String search) {
    	Map<String, String> namedParameters = new HashMap<>();
    	 String sql = null;
		namedParameters.put("id", id.toString());
    	if(search!=null&&search.trim().length()!=0){
        sql = "select distinct m.* from messages m inner join followers f on m.person_id = f.person_id where (m.person_id = :id or (m.person_id = f.person_id and f.follower_person_id = :id)) and m.content like :search";
    	namedParameters.put("search","%" + search + "%");
    	}
    	else{
    	sql = "select distinct m.* from messages m inner join followers f on m.person_id = f.person_id where (m.person_id = :id or (m.person_id = f.person_id and f.follower_person_id = :id))";  
    	}
    	SqlParameterSource name =new MapSqlParameterSource(namedParameters);
	    	
		return this.namedParameterJdbcTemplate.query(sql, name, new MessagesMapper());
	}
    // Query the current user' followers, return a list of followers
	@Transactional
	public List<People> getFollowers(Integer id) {
		Map<String, Integer> namedParameters = new HashMap<>();
		namedParameters.put("id", Integer.valueOf(id));
	    String sql = "select *  from people where id in (select follower_person_id from followers where person_id = :id)";
	    SqlParameterSource name =new MapSqlParameterSource(namedParameters);	
		return this.namedParameterJdbcTemplate.query(sql, name, new PeopleMapper());
    }
	 // Query the list of people current user followed, return a list of people
	@Transactional
	public List<People> getFollowingList(Integer id) {
		 String query = "select * from people where id in (select person_id from followers where follower_person_id = :id)";
	        Map<String, Integer> namedParameters = new HashMap<>();
	        namedParameters.put("id", Integer.valueOf(id));
		    SqlParameterSource name =new MapSqlParameterSource(namedParameters);	
	        return this.namedParameterJdbcTemplate.query(query, name, new PeopleMapper());
	}
	
	//following a user by id, first check if current user is already followed this person, if not, update the follower table in the database 
	// if yes, then we do not want insert a duplicate info to follower table
    @Transactional
    public void following(Integer followerId, Integer id) {
        Map<String, Integer> namedParameters = new HashMap<>();
        namedParameters.put("id", id);
        namedParameters.put("followerId", followerId);
        SqlParameterSource name = new MapSqlParameterSource(namedParameters);

        String sql = "Select * from followers f where f.person_id = :id and f.follower_person_id = :followerId";
        List<Followers> check = this.namedParameterJdbcTemplate.query(sql, namedParameters, new FollowersMapper());
        if ((check.size() == 0)) {
            String query = "Insert into followers (person_id,follower_person_id) values (:id, :followerId)";
            this.namedParameterJdbcTemplate.update(query, name);
        }
    }
    // unfollow use by userId, delete the row that match the info that user and the person user follow
    @Transactional
	public void deleteUserFollowing(Integer followerId, Integer id) {
    	 String query = "Delete from followers where person_id = :id and follower_person_id = :followerId";
         Map<String, Integer> namedParameters = new HashMap<>();
         namedParameters.put("id", id);
         namedParameters.put("followerId", followerId);
         SqlParameterSource name = new MapSqlParameterSource(namedParameters);
         this.namedParameterJdbcTemplate.update(query, name);
		
	}


    // Getting number of followers for each user  (used in extra fearture: 3)
    @Transactional
  	public List<UserIdStats>  getUserIdWithNumOfFollowers() {
  		Map<String, Integer> namedParameters = new HashMap<>();
  	    String sql = "select p.id, count(*) AS fan_count from people p left join followers f on p.id = f.person_id group by p.id, p.name order by p.id";
  	    SqlParameterSource name =new MapSqlParameterSource(namedParameters);  	   	    
  		return  this.namedParameterJdbcTemplate.query(sql, name, new UserIdStatsMapper());
  	}
    // Getting number of people  each of user follow (used in extra fearture: 3)
    @Transactional
   	public List<UserIdStats>  getUserIdWithNumOfFollowering() {
   		Map<String, Integer> namedParameters = new HashMap<>();
   	    String sql = "select p.id, count(*) AS fan_count from people p left join followers f on p.id = f.follower_person_id group by p.id, p.name order by p.id";
   	    SqlParameterSource name =new MapSqlParameterSource(namedParameters);  	   	    
   		return  this.namedParameterJdbcTemplate.query(sql, name, new UserIdStatsMapper());
   	}
    


    //get a list of user and their most popular follower (used in extra fearture: 2)
	public List<PopularFollowers> getPopularFollowers() {
		// TODO Auto-generated method stub
		Map<String, Integer> namedParameters = new HashMap<>();
		String sql = "select person.id, person.name, max(rslt.fan_count) AS max_fan_count from people person left join followers flers on flers.person_id = person.id left join (select p.id, count(*) AS fan_count from people p left join followers f on p.id = f.person_id group by p.id) rslt on flers.follower_person_id = rslt.id group by person.id, person.name order by person.id;";
		SqlParameterSource name =new MapSqlParameterSource(namedParameters);	
		return this.namedParameterJdbcTemplate.query(sql, name,new PopularFollowersMapper());
	}
	
}
	
