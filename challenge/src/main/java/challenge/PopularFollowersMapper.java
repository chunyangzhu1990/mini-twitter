package challenge;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class PopularFollowersMapper implements RowMapper<PopularFollowers> {


	@Override
	public PopularFollowers mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		PopularFollowers people = new PopularFollowers();
		people.setCount(rs.getInt("max_fan_count"));
		people.setId(rs.getInt("id"));
		people.setName(rs.getString("name"));
		return people;
	}

}
