package challenge;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class UserIdStatsMapper implements RowMapper<UserIdStats> {

	@Override
	public UserIdStats mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		
		UserIdStats rst = new UserIdStats();
		rst.setX(rs.getInt("id"));
		rst.setY(rs.getInt("fan_count"));
		return rst;
	}

}
