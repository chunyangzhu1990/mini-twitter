package challenge;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageController {
    @Autowired
    private Database database;
	
	
    @RequestMapping(value = "/messages", method = RequestMethod.GET)
    @ResponseBody
    public List<Messages> test(HttpServletRequest request) {
    	Principal principal = request.getUserPrincipal();
    	Integer id = Integer.parseInt(principal.getName());
         return database.getUserMessages(id,request.getHeader("search"));
    }
    
}
