package challenge;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class PeopleMapper implements RowMapper<People> {

	@Override
	public People mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		
		People people = new People();
		people.setId(rs.getInt("id"));
		people.setName(rs.getString("name"));
		people.setHandle(rs.getString("handle"));
		return people;
	}

}
