package challenge;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class FollowersMapper implements RowMapper<Followers> {

	@Override
	public Followers mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		Followers follower = new Followers();
		follower.setId(rs.getInt("id"));
		follower.setFollower_person_id(rs.getInt("follower_person_id"));
		follower.setPerson_id(rs.getInt("person_id"));
		
		return follower;
	}

}
