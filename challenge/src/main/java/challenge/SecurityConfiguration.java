package challenge;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;



@Configuration
@EnableWebSecurity

public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private AuthenticationEntryPoint auth;
	 @Autowired
	private DataSource dataSource;
	 
	 
	    @Override
	    protected void configure(HttpSecurity http) throws Exception {
	        http.authorizeRequests()
	          .antMatchers("/h2-console/*").permitAll().anyRequest().authenticated()
	          .and().formLogin().usernameParameter("id").passwordParameter("handle")
	          .and()
	          .httpBasic()
	          .authenticationEntryPoint(auth);
	 
	        http.csrf().disable();
	        http.headers().frameOptions().disable();

	  }
	// configuring the HTTP Basic authentication, make a loggin function, in people table, id will be used as username, and handle will be used as password
	// for example:    Username: 1, Password: batman    
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
            auth.jdbcAuthentication().dataSource(dataSource)
                    .usersByUsernameQuery("select id as username, handle as password, true from people where id = ?")
                    .authoritiesByUsernameQuery(
                            "select id as username,'ROLE_USER' as role from people where id=?");

        }
 

	

}